import { User } from "./user.model";

export class Chat {
    key: string
    name: string
    createdAt: Date
    participantsStr?: string
    members: User[]
    status?: string
}