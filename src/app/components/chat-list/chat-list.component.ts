import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AngularFireDatabase, AngularFireList, QueryFn } from 'angularfire2/database';

import { Message } from '../../models/message.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../models/user.model';
import { Subscription, forkJoin, Observable, pipe,  } from 'rxjs';
import { Chat } from '../../models/chat.model';
import { tap, catchError, map, mergeMap, take } from 'rxjs/operators';
import { combineLatest, merge, concat } from 'rxjs'
import * as firebase from 'firebase'

import { trigger, query, transition, style, animate } from '@angular/animations'

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.css'],
  animations: [trigger('testAnimation', [
    transition('* => *', [
      query(':enter', [
        style({transform: 'translateY(-10%)', opacity: 0}),
        animate('0.3s', style(
          {transform: 'translateY(0)', opacity: 1}
        ))
      ], {optional: true})
    ])
  ])]
})
export class ChatListComponent implements OnInit {
  user: User
  subs: Subscription[] = []
  chats$: Observable<any[]>
  chatsLength: number
  selectedChat: string
  constructor(private afDb: AngularFireDatabase, private afAuth: AngularFireAuth, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.afAuth.user.subscribe(
      user => {
        this.user = user
        this.getAllChats()
      }
    )
  }

  mockData() {
    const chat: Chat = new Chat()
    chat.name = 'Public Chat 1'
    chat.status = 'public'
    chat.createdAt = new Date()
    this.afDb.list('chats').push(chat)
  }
  mockDataUser() {
    const chat: Chat = new Chat()
    chat.name = 'Private Chat 1'
    chat.status = 'private'
    chat.createdAt = new Date()
    chat.participantsStr = this.user.uid
    this.afDb.list('chats').push(chat)
  }

  getAllChats() {
    this.chats$ = combineLatest(this.getPublicChats(), this.getUserChats()).pipe(
      map(res => {
        const chats = [...res[0], ...res[1]]
        return chats
      })
    )
  }

  getUserChats(): Observable<any[]> {
    return this.afDb.list('chats', ref => {
      console.log(this.user.uid)
      return ref.orderByChild('participantsStr').startAt(this.user.uid)
    })
    .snapshotChanges().pipe(
      map(actions => {
        const chats = actions.map(a => ({ key: a.key, ...a.payload.val() }))
        return  chats.filter((item: Chat) => item.participantsStr.indexOf(this.user.uid) >= 0)
      })
    )
  }

  getPublicChats(): Observable<any[]> {
    return this.afDb.list('chats', ref => {
      return ref.orderByChild('status').equalTo('public')
    })
    .snapshotChanges().pipe(
      map(actions => {
        return  actions.map(a => ({ key: a.key, ...a.payload.val() }))
      })
    )
  }
}
