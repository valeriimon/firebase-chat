import { Component, OnInit, OnChanges, Input, ElementRef } from '@angular/core';
import { Message } from '../../models/message.model';
import { FirebaseDatabase } from 'angularfire2';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Chat } from '../../models/chat.model';
import { Observable, Subscription } from 'rxjs';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../models/user.model';
import { map, tap } from 'rxjs/operators';
import { pick } from 'lodash'
import { AnimationBuilder, query, animate, style, AnimationFactory, AnimationPlayer, sequence, keyframes } from '@angular/animations';

@Component({
  selector: 'app-chat-thread',
  templateUrl: './chat-thread.component.html',
  styleUrls: ['./chat-thread.component.css']
})
export class ChatThreadComponent implements OnChanges {
  @Input() chat: Chat
  chatAnimation: AnimationPlayer
  private offerForm: FormGroup
  message: Message = new Message()
  offerAttached: boolean
  messageList$: Observable<any[]>
  user: User
  subs: Subscription[] = []
  constructor(
    private afDb: AngularFireDatabase, 
    private fb: FormBuilder, 
    private afAuth: AngularFireAuth,
    private _element: ElementRef,
    private _builder: AnimationBuilder) {
    this.initOfferForm()
  }

  ngOnInit() {
    this.user = pick(this.afAuth.auth.currentUser, ['uid', 'email', 'displayName', 'phoneNumber', 'photoUrl'])
    this._animationBuilder()
  }

  ngOnChanges() {
    if(!this.chat) return
    this.chatAnimation.play()
    this.messageList$ = this.afDb.list<Message[]>('messages', ref => {
      return ref.orderByChild('chatRef').equalTo(this.chat.key)
    }).snapshotChanges().pipe(
      map(actions => {
        return  actions.map(a => ({ key: a.key, ...a.payload.val() }))
      })
    )
    
  }

  _animationBuilder() {
    this.chatAnimation = this._builder.build([
      query('.chat-messages > .feed', [
        sequence([
          animate('0.8s ease-in-out', keyframes([
            style({
              transform: 'rotate(100deg) scale(1.01)',
              offset: 0.1
            }),
            style({
              transform: 'rotate(800deg) scale(0)',
              offset: 1
            }),
          ])),
          animate('0.3s ease-in', style({
            transform: 'rotate(0deg) scale(1)',
          }))
        ])
        
      ])
    ]).create(this._element.nativeElement)
  }

  initOfferForm() {
    this.offerForm = this.fb.group({
      offerDescription: ['', Validators.required],
      offerAmount: [1000, Validators.minLength(100)]
    })
  }

  sendMessage() {
    if(this.offerAttached) {
      if(this.offerForm.invalid) {
        return
      }
      this.message.offer = this.offerForm.value
    }
    
    this.message.sender = this.user.uid
    this.message.chatRef = this.chat.key
    
    this.afDb.list('messages').push(this.message)
  }

}
