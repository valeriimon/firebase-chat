import { Component, OnInit, Input } from '@angular/core';
import { Message } from '../../models/message.model';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthService } from '../../services/auth/auth.service';
import { User } from '../../models/user.model';
import { take } from 'rxjs/operators'

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  @Input() msg: Message
  user: User
  sender: User
  constructor(private authService: AuthService, private afDb: AngularFireDatabase) { }
  
  get isOwner() {
    return this.sender && this.sender.uid === this.user.uid
  }
  
  ngOnInit() {
    this.user = this.authService.getUser()
    this.getSender()
    
  }

  getSender() {
    this.afDb.list('users', ref => ref.orderByChild('uid').equalTo(this.msg.sender))
    .valueChanges().pipe(
      take(1)
    ).subscribe(([user]: User[]) => {
      this.sender = user
      console.log(user)
    })
  }

}
